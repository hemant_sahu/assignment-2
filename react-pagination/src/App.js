import React from 'react';
import Header from './Header';
import ContentSection from './ContentSection';
import './App.css';

function App() {
  return (
    <div className="App">
      <Header/>
      <ContentSection/>
    </div>
  );
}

export default App;
