import React from 'react';
import Control from './Control';

const contentSection = () => (
    <>
        <main>
          <div class="list" id="list"></div>
          <Control/>
        </main>
    </>
);

export default contentSection;