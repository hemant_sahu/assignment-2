import React from 'react';

const displayNoOfItem = () => (
    <>
        <select name="" id="num_of_item">
            <option value="10">10</option>
            <option value="20">20</option>
        </select>
    </>
);

export default displayNoOfItem;