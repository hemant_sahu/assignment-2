import React from 'react';

const header = () => (
    <header>
        <h1>Student Details</h1>
    </header>
);

export default header;