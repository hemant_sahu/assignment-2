import React, {Component} from 'react';
import Button from './Button';
import DisplayPageNO from './DisplayPageNo';
import DisplayNoOfItem from './DisplayNoOfItem';

class Control extends Component {

    render(){
        return(
            <>
                <div class="btn">
                    <Button>First</Button>
                    <Button>Prev</Button>
                    <DisplayPageNO/>
                    <Button>Next</Button>
                    <Button>Last</Button>
                    <DisplayNoOfItem/>
                </div>
            </>
        );
    };
};

export default Control;